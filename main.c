// Compilar com $ gcc -Wall -pg -lpthread main.c -o vetor
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_THREADS 4
#define ESCALAR 100

unsigned int *vetor;

typedef struct argument {
    int id;
    unsigned int size;
    unsigned int *vetor;
} argument;

void FillVector(unsigned int *vetor, unsigned int size) {
    unsigned int i;

    for (i = 0; i < size; i++) {
        vetor[i] = i;
    }
}

void *MultiplyVector(void *arg) {
    argument *elem;
    unsigned int i;
    // unsigned int i, lower, upper;

    elem = (argument *)arg;

    // lower = elem->id * elem->size;
    // upper = ((elem->id + 1) * elem->size);

    for (i = 0; i < elem->size; i++) {
        elem->vetor[i] = elem->vetor[i] * ESCALAR;
    }

    // printf("Thread ID: %d Size: %u Lower: %u Upper: %u\n", elem->id, elem->size, lower, upper);  //, vetor[lower]);
    printf("Thread ID: %d Size: %u\n", elem->id, elem->size);

    return NULL;
}

int FindMax(unsigned int *vetor, unsigned int size) {
    unsigned int i, max = 0;

    for (i = 0; i < size; i++) {
        if (vetor[i] > max) {
            max = vetor[i];
        }
    }

    return max;
}

void PrintVector(unsigned int *vetor, unsigned int size) {
    unsigned int i;

    printf("Vetor: ");
    for (i = 0; i < size; i++) {
        printf(" %u", vetor[i]);
    }
    printf("\n");
}

int main(int argc, char *argv[]) {
    pthread_t threads[NUM_THREADS];
    argument *thread_args;
    unsigned int *vetor;
    // unsigned int size, max, i;
    unsigned int size, tmp_size, i;

    if (argc != 2) {
        printf("Numero invalido de elementos\n");
        return -1;
    }

    size = atoi(argv[1]);
    vetor = (unsigned int *)malloc(sizeof(unsigned int) * size);
    thread_args = (argument *)malloc(sizeof(argument) * size);

    FillVector(vetor, size);
    PrintVector(vetor, size);

    tmp_size = size / NUM_THREADS;
    for (i = 0; i < NUM_THREADS; i++) {
        thread_args[i].id = i;
        thread_args[i].size = tmp_size;
        thread_args[i].vetor = &vetor[i * thread_args[i].size];
        pthread_create(&threads[i], NULL, MultiplyVector, (void *)&thread_args[i]);
    }

    for (i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    // MultiplyVector(vetor, size);
    PrintVector(vetor, size);

    // max = FindMax(vetor, size);
    // printf("MAX: %d\n", max);

    free(vetor);

    return 0;
}
